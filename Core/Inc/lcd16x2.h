#include "stm32f1xx_hal.h"
#pragma once

typedef struct {
    GPIO_TypeDef* Port;
    uint16_t Pin;
} LCD_GPIOPair;

typedef struct {
    LCD_GPIOPair RS;
    LCD_GPIOPair EN;
    LCD_GPIOPair DB4;
    LCD_GPIOPair DB5;
    LCD_GPIOPair DB6;
    LCD_GPIOPair DB7;
} LCD_HandleTypeDef;

// DDRAM address of first char of line 1
#define LCD16X2_START_LINE_1	0x00
// DDRAM address of first char of line 2
#define LCD16X2_START_LINE_2	0x40

/** Instructions bit location ----------------------------------------------- */
#define LCD16X2_CLEAR_DISPLAY					0x01
#define LCD16X2_CURSOR_HOME						0x02
#define LCD16X2_CHARACTER_ENTRY_MODE	0x04
#define LCD16X2_DISPLAY_CURSOR_ON_OFF	0x08
#define LCD16X2_DISPLAY_CURSOR_SHIFT 	0x10
#define LCD16X2_FUNCTION_SET					0x20
#define LCD16X2_SET_CGRAM_ADDRESS	 		0x40
#define LCD16X2_SET_DDRAM_ADDRESS	 		0x80
/* Character entry mode instructions */
#define LCD16X2_INCREMENT							0x02	// Initialization setting
#define LCD16X2_DECREMENT							0x00
#define LCD16X2_DISPLAY_SHIFT_ON			0x01
#define LCD16X2_DISPLAY_SHIFT_OFF			0x00	// Initialization setting
/* Display cursor on off instructions */
#define LCD16X2_DISPLAY_ON	 					0x04
#define LCD16X2_DISPLAY_OFF	 					0x00	// Initialization setting
#define LCD16X2_CURSOR_UNDERLINE_ON	 	0x02
#define LCD16X2_CURSOR_UNDERLINE_OFF	0x00	// Initialization setting
#define LCD16X2_CURSOR_BLINK_ON	 			0x01
#define LCD16X2_CURSOR_BLINK_OFF	 		0x00	// Initialization setting
/* Display cursor shift instructions */
#define LCD16X2_DISPLAY_SHIFT					0x08
#define LCD16X2_CURSOR_MOVE						0x00
#define LCD16X2_RIGHT_SHIFT						0x04
#define LCD16X2_LEFT_SHIFT						0x00
/* Function set instructions */
#define LCD16X2_8BIT_INTERFACE				0x10	// Initialization setting
#define LCD16X2_4BIT_INTERFACE				0x00
#define LCD16X2_2LINE_MODE						0x08
#define LCD16X2_1LINE_MODE						0x00	// Initialization setting
#define LCD16X2_5X10DOT_FORMAT				0x04
#define LCD16X2_5X7DOT_FORMAT					0x00	// Initialization setting
/* Busy flag bit location */
#define LCD16X2_BUSY_FLAG							0x80

void LCD_sendCmd(LCD_HandleTypeDef *lcd, uint8_t data);

void LCD_sendChar(LCD_HandleTypeDef *lcd, uint8_t data);

void LCD_sendNibble(LCD_HandleTypeDef *lcd, uint8_t data);

void LCD_sendByte(LCD_HandleTypeDef *lcd, uint8_t data);

void LCD_puts(LCD_HandleTypeDef *lcd, char * data);

void LCD_Init(LCD_HandleTypeDef *lcd);

void LCD_clrscr(LCD_HandleTypeDef *lcd);

void LCD_home(LCD_HandleTypeDef *lcd);

void LCD_gotoxy(LCD_HandleTypeDef *lcd, uint8_t x, uint8_t y);