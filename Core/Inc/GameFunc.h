/*
 * GameFunc.h
 *
 *  Created on: Jun 30, 2020
 *      Author: sub
 */

#ifndef INC_GAMEFUNC_H_
#define INC_GAMEFUNC_H_

#include "lcd16x2.h"
#include "time.h"


uint8_t RandomPlace(uint8_t from,uint8_t to);
void StartCountDown(LCD_HandleTypeDef *lcd,uint32_t delay);
void PrintOneCharacter(LCD_HandleTypeDef *lcd,char *character);
void FirstLevelGround(LCD_HandleTypeDef *lcd,uint16_t cols,uint16_t ground);


void  CreateCustomCharacter(LCD_HandleTypeDef *lcd,unsigned char *character,uint8_t counter);
void PrintCustomCharacter(LCD_HandleTypeDef *lcd,uint8_t character_number,uint32_t delay);


void ScrollingText(LCD_HandleTypeDef *lcd,const char* text);




#endif /* INC_GAMEFUNC_H_ */
