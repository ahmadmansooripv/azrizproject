#include "lcd16x2.h"

void LCD_sendCmd(LCD_HandleTypeDef *lcd, uint8_t data) {
    HAL_GPIO_WritePin(lcd->RS.Port, lcd->RS.Pin, GPIO_PIN_RESET);
    LCD_sendByte(lcd, data);
}

void LCD_sendChar(LCD_HandleTypeDef *lcd, uint8_t data) {
    HAL_GPIO_WritePin(lcd->RS.Port, lcd->RS.Pin, GPIO_PIN_SET);
    LCD_sendByte(lcd, data);
}

void LCD_sendNibble(LCD_HandleTypeDef *lcd, uint8_t data) {

    HAL_GPIO_WritePin(lcd->EN.Port, lcd->EN.Pin, GPIO_PIN_SET);
    HAL_Delay(1);
    HAL_GPIO_WritePin(lcd->DB4.Port, lcd->DB4.Pin, !!(data & 0x01) ? GPIO_PIN_SET : GPIO_PIN_RESET);
    HAL_GPIO_WritePin(lcd->DB5.Port, lcd->DB5.Pin, !!(data & 0x02) ? GPIO_PIN_SET : GPIO_PIN_RESET);
    HAL_GPIO_WritePin(lcd->DB6.Port, lcd->DB6.Pin, !!(data & 0x04) ? GPIO_PIN_SET : GPIO_PIN_RESET);
    HAL_GPIO_WritePin(lcd->DB7.Port, lcd->DB7.Pin, !!(data & 0x08) ? GPIO_PIN_SET : GPIO_PIN_RESET);
    HAL_Delay(1);
    HAL_GPIO_WritePin(lcd->EN.Port, lcd->EN.Pin, GPIO_PIN_RESET);
}

void LCD_sendByte(LCD_HandleTypeDef *lcd, uint8_t data) {
    LCD_sendNibble(lcd, data >> 4); // High order bit
    LCD_sendNibble(lcd, data); // Low order bit
}

void LCD_puts(LCD_HandleTypeDef *lcd, char * data) {
    while (data[0] != '\0') {
        LCD_sendChar(lcd, data[0]);
        data++;
    }
}

void LCD_Init(LCD_HandleTypeDef *lcd)
{
  HAL_Delay(100);
  LCD_sendCmd(lcd, 0x33); // Initialize controller
  LCD_sendCmd(lcd, 0x32); // Set 4-bit mode
  LCD_sendCmd(lcd, 0x28); // 4 bit, 2 line, 5x7
  LCD_sendCmd(lcd, 0x06); // Cursor direction -> right
  LCD_sendCmd(lcd, 0x0C); // Display on, cursor off
  LCD_sendCmd(lcd, 0x01); // Clear display

  HAL_Delay(100);
}

void LCD_clrscr(LCD_HandleTypeDef *lcd)
{
	LCD_sendCmd(lcd, LCD16X2_CLEAR_DISPLAY);
}

void LCD_home(LCD_HandleTypeDef *lcd)
{
	LCD_sendCmd(lcd, LCD16X2_CURSOR_HOME);
}

void LCD_gotoxy(LCD_HandleTypeDef *lcd, uint8_t x, uint8_t y)
{
	if (y == 0)
		LCD_sendCmd(lcd, LCD16X2_SET_DDRAM_ADDRESS | (LCD16X2_START_LINE_1 + x));
	else
		LCD_sendCmd(lcd, LCD16X2_SET_DDRAM_ADDRESS | (LCD16X2_START_LINE_2 + x));
}
