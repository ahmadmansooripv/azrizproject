
#include"GameFunc.h"


// LCD manipulating code

// lcd : LCD_HandleTypeDef object pointer from lcd16x2.h library
// character : pointer to the first row of 8-row char[]
// EXAMPLE : unsigned char custom_character[] = {0x0e, 0x0e, 0x04, 0x04, 0x1f, 0x04, 0x0a, 0x0a} ---- simple body
// counter : CGram is 64byte and each character is 8 byte, only 8 character is possible, counter is from 0 to 7
void CreateCustomCharacter(LCD_HandleTypeDef *lcd,unsigned char *character,uint8_t counter){
	if ( counter >= 8)
		return;
	LCD_sendCmd(lcd,LCD16X2_SET_CGRAM_ADDRESS + counter*8 );		// set data in character generated ram

	for (int i=0; i<8; i++){
		 LCD_sendChar(lcd,*character);        // passing each row of 8-row character to lcd ram
		 character++;						  // set character pointer to next row
	}

}


// character_number: for first character that you registered in CGRam pass 0 , for next pass 1 and ... until 8
void PrintCustomCharacter(LCD_HandleTypeDef *lcd,uint8_t character_number,uint32_t delay){
	LCD_sendChar(lcd,character_number);
	HAL_Delay(delay);
}




// GAME DATA STRUCTURE

struct Position{
	uint8_t row;
	uint8_t col;
};

struct Block{
	struct Position pos;
	struct Block* next;
};

struct SpawnedBlock{
	struct Block head;
	struct Block tail;

};




uint8_t RandomPlace(uint8_t from,uint8_t to){
	return from + rand() % (to - from);
}

void PrintOneCharacter(LCD_HandleTypeDef *lcd,char *character){

	HAL_GPIO_WritePin(lcd->RS.Port, lcd->RS.Pin, GPIO_PIN_SET);

	 LCD_sendNibble(lcd,(int)character>>4);
	 LCD_sendNibble(lcd,(int)character);

	 HAL_GPIO_WritePin(lcd->RS.Port, lcd->RS.Pin, GPIO_PIN_SET);
}


// function for Game Start Screen # delay in ms
void StartCountDown(LCD_HandleTypeDef *lcd,uint32_t delay){

	for(int i =3;i>0;i--){
		LCD_gotoxy(lcd,0x07,0);
		PrintCustomCharacter(lcd,i-1,500);

	}

	 LCD_clrscr(lcd);
	 LCD_gotoxy(lcd, 0x05, 0x0);
	 LCD_puts(lcd, "START!");
	 HAL_Delay(delay);

}


void FirstLevelGround(LCD_HandleTypeDef *lcd,uint16_t cols,uint16_t ground){
	unsigned char block = 0b11111111;
	for(int i=0;i<cols;i++){
		LCD_gotoxy(lcd,i,ground);
		PrintOneCharacter(lcd,block);
	}
}


void ScrollingText(LCD_HandleTypeDef *lcd,const char* text){
	size_t length = strlen(text);
	LCD_clrscr(lcd);
	int semi_j=0;
	int rtl = 1;
	while(1){
//		int j=semi_j;

		if (rtl){
			for(int i=15;i>0;i--){
				for(int j=15,count =15-i;j>=i && count>=0;j--,count--){
					LCD_gotoxy(lcd,j,0x0);
					LCD_sendChar(lcd,text[count]);

				}
			}
			rtl =!rtl;
		}

		else{

			int counter=0;
			for(int i=0;i<length;i++){
				LCD_gotoxy(lcd,0x0,0x0);
				for(int j=0;j<16 && counter+j<length;j++){
						LCD_sendChar(lcd,text[counter+j]);

				}
				if (counter==length-1) rtl=!rtl;
				counter++;
			}

//		LCD_clrscr(lcd);
//		HAL_Delay(1000);

		}

	}
}



